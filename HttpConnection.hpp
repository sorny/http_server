#ifndef HTTP_CONNECTION_H
#define HTTP_CONNECTION_H

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/core/string.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <iostream>
#include <unordered_map>

namespace beast = boost::beast;   // from <boost/beast.hpp>
namespace http = beast::http;     // from <boost/beast/http.hpp>
namespace net = boost::asio;      // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp; // from <boost/asio/ip/tcp.hpp>

using responseFunc =
        http::response<http::dynamic_body> (*)(const http::request<http::dynamic_body> &);

struct pair_hash {
    template<class T1, class T2>
    std::size_t operator()(const std::pair<T1, T2> &pair) const {
        return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
    }
};

class http_connection : public std::enable_shared_from_this<http_connection> {
public:
    http_connection(tcp::socket socket) : socket_(std::move(socket)) {}

    static void add_endpoint(const std::string &endpoint, const std::string &method,
                             responseFunc res) {
        auto method_ = http::verb::unknown;
        if (method == "GET") {
            method_ = http::verb::get;
        } else if (method == "POST") {
            method_ = http::verb::post;
        } else {
            std::cerr << "Only GET and POST are available. Endpoint " << endpoint
                      << " is not going to be used." << std::endl;
            return;
        }
        endpoints_.insert({{endpoint, method_}, res});
    }

    // Initiate the asynchronous operations associated with the connection.
    void start();

private:
    // The socket for the currently connected client.
    tcp::socket socket_;

    static std::unordered_map<std::pair<std::string, http::verb>, responseFunc, pair_hash>
            endpoints_;

    // The buffer for performing reads.
    beast::flat_buffer buffer_{8192};

    // The request message.
    http::request<http::dynamic_body> request_;

    // The response message.
    http::response<http::dynamic_body> response_;

    // The timer for putting a deadline on connection processing.
    net::steady_timer deadline_{socket_.get_executor(), std::chrono::seconds(60)};

    // Asynchronously receive a complete request message.
    void read_request();

    // Determine what needs to be done with the request message.
    void process_request();

    // Construct a response message based on the program state.
    void create_response();

    // Asynchronously transmit the response message.
    void write_response();

    // Check whether we have spent enough time on this connection.
    void check_deadline();
};


#endif //HTTP_CONNECTION_H
