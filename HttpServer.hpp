//
// Created by esteve on 15/6/20.
//

#ifndef HTTPCLIENT_BEAST_HTTPSERVER_HPP
#define HTTPCLIENT_BEAST_HTTPSERVER_HPP

#include <memory>
#include <string>

#include "HttpConnection.hpp"
#include <boost/asio.hpp>
#include <boost/asio/ip/address.hpp>

namespace net = boost::asio; // from <boost/asio.hpp>


class HttpServer {
public:
    HttpServer(const std::string &server_address, unsigned short port);

    void accept_connection();
    void start();
    void stop();

    void endpoint(const std::string &endpoint, const std::string &method,
                  responseFunc response);

private:
    net::io_context ioc_{1};
    std::unique_ptr<boost::asio::ip::tcp::acceptor> acceptor_;
    std::unique_ptr<boost::asio::ip::tcp::socket> socket_;
    boost::asio::ip::address address_;
    unsigned short port_{0};
};


#endif //HTTPCLIENT_BEAST_HTTPSERVER_HPP
