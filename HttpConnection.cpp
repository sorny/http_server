#include "HttpConnection.hpp"

#include <iostream>


std::unordered_map<std::pair<std::string, http::verb>, responseFunc, pair_hash>
        http_connection::endpoints_;

void http_connection::start() {
    read_request();
    check_deadline();
}
void http_connection::read_request() {
    auto self = shared_from_this();

    http::async_read(socket_, buffer_, request_,
                     [self](beast::error_code ec, std::size_t bytes_transferred) {
                         boost::ignore_unused(bytes_transferred);
                         if (!ec) {
                             self->process_request();
                         }
                     });
}
void http_connection::process_request() {
    response_.version(request_.version());
    response_.keep_alive(false);

    create_response();
    write_response();
}
void http_connection::create_response() {
    //TODO: CASTING string_view to string is a deep copy and not efficient.
    // Needs to be fixed
    auto endpoint = endpoints_.find({std::string(request_.target()), request_.method()});
    if (endpoint != endpoints_.end()) {
        auto method = endpoint->first.second;
        if (request_.method() != method) {
            // We return responses indicating an error if
            // we do not recognize the request method.
            response_.result(http::status::bad_request);
            response_.set(http::field::content_type, "text/plain");
            beast::ostream(response_.body())
                    << "Invalid request-method '" << std::string(request_.method_string())
                    << "'\r\n";
        } else {
            auto lambda_response = endpoint->second;
            response_ = lambda_response(request_);
        }
        return;
    }
    response_.result(http::status::not_found);
    response_.set(http::field::content_type, "text/plain");
    beast::ostream(response_.body()) << "File not found\r\n";
}
void http_connection::write_response() {
    auto self = shared_from_this();

    response_.set(http::field::content_length, response_.body().size());

    http::async_write(socket_, response_, [self](beast::error_code ec, std::size_t /*unused*/) {
        self->socket_.shutdown(tcp::socket::shutdown_send, ec);
        self->deadline_.cancel();
    });
}
void http_connection::check_deadline() {
    auto self = shared_from_this();
    deadline_.async_wait([self](beast::error_code ec) {
        if (!ec) {
            // Close socket to cancel any outstanding operation.
            self->socket_.close(ec);
        }
    });
}
