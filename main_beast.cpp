//
// Created by esteve on 15/6/20.
//

#include "HttpServer.hpp"
#include <iostream>


namespace my_program_state {
    std::size_t request_count() {
        static std::size_t count = 0;
        return ++count;
    }

    std::time_t now() { return std::time(0); }
} // namespace my_program_state

int main(int argc, char *argv[]) {
    try {
        // Check command line arguments.
        if (argc != 3) {
            std::cerr << "Usage: " << argv[0] << " <address> <port>\n";
            std::cerr << "  For IPv4, try:\n";
            std::cerr << "    receiver 0.0.0.0 80\n";
            std::cerr << "  For IPv6, try:\n";
            std::cerr << "    receiver 0::0 80\n";
            return EXIT_FAILURE;
        }

        auto test_get_request = [](const http::request<http::dynamic_body> &request) {
            http::response<http::dynamic_body> response;
            response.result(http::status::ok);
            response.set(http::field::content_type, "text/html");
            beast::ostream(response.body())
                    << "<html>\n"
                    << "<head><title>Request count</title></head>\n"
                    << "<body>\n"
                    << "<h1>Time</h1>\n"
                    << "<p>It's " << my_program_state::now() << " seconds since 1970.</p>\n"
                    << "</body>\n"
                    << "</html>\n";
            return response;
        };

        auto test_post_request = [](const http::request<http::dynamic_body> &request) {
            http::response<http::dynamic_body> response;
            response.result(http::status::ok);
            response.set(http::field::content_type, "text/html");
            beast::ostream(response.body())
                    << "<html>\n"
                    << "<head><title>Request count</title></head>\n"
                    << "<body>\n"
                    << "<h1>Time</h1>\n"
                    << "<p>It's " << my_program_state::now()
                    << " seconds since 1970. ALSO I'm a POST REQUEST</p>\n"
                    << "</body>\n"
                    << "</html>\n";
            return response;
        };

        auto count_get_request = [](const http::request<http::dynamic_body> &request) {
            http::response<http::dynamic_body> response;
            response.result(http::status::ok);
            response.set(http::field::content_type, "text/html");
            beast::ostream(response.body())
                    << "<html>\n"
                    << "<head><title>Request count</title></head>\n"
                    << "<body>\n"
                    << "<h1>Request count</h1>\n"
                    << "<p>There have been " << my_program_state::request_count()
                    << " requests so far.</p>\n"
                    << "</body>\n"
                    << "</html>\n";
            return response;
        };

        HttpServer server(argv[1], atoi(argv[2]));
        server.endpoint("/test", "GET", test_get_request);
        server.endpoint("/test", "POST", test_post_request);
        server.endpoint("/count", "GET", count_get_request);
        server.start();

    } catch (std::exception const &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
