//
// Created by esteve on 15/6/20.
//

#include "HttpServer.hpp"

#include "HttpConnection.hpp"
#include <boost/asio.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>

namespace beast = boost::beast;           // from <boost/beast.hpp>
namespace http = beast::http;             // from <boost/beast/http.hpp>
namespace net = boost::asio;              // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;         // from <boost/asio/ip/tcp.hpp>
using address = boost::asio::ip::address; // from <boost/asio/ip/address.hpp>

HttpServer::HttpServer(const std::string &server_address, unsigned short port) {
    address_ = address{net::ip::make_address(server_address)};
    port_ = port;
    acceptor_ = std::make_unique<tcp::acceptor>(tcp::acceptor{ioc_, {address_, port_}});
    socket_ = std::make_unique<tcp::socket>(tcp::socket{ioc_});
    accept_connection();
}

void HttpServer::accept_connection() {
    acceptor_->async_accept(*socket_, [&](beast::error_code ec) {
        if (!ec) {
            std::make_shared<http_connection>(std::move(*socket_))->start();
        }
        accept_connection();
    });
}

void HttpServer::start() {
    ioc_.run();
}
void HttpServer::stop() {
    if (ioc_.stopped()) {
        ioc_.run();
    }
}
void HttpServer::endpoint(const std::string &endpoint, const std::string &method,
                          responseFunc response) {
    http_connection::add_endpoint(endpoint, method, response);
}
